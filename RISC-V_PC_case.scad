//@SECTION: config
$fn = 99; //@NOTE: invalid 2-manifold work-around
//@SECTION: components
module guts()
{
    //@SECTION: components
    module expansion_board()
    {
        //@SECTION: constants
        x = 272;
        y = 120;
        z = 2;
        standoff_z = 18;
        standoff_points = [
            [-x/2 + 7, -y/2 + 4],
            [-x/2 + 7, y/2 - 4],
            [-x/2 + 7, -y/2 + 45.5],
            [-x/2 + 31.5, -y/2 + 4],
            [-x/2 + 31.5, y/2 - 4],
            [-x/2 + 31.5, -y/2 + 45.5],
            [-x/2 + 69.75, -y/2 + 4],
            [-x/2 + 69.75, y/2 - 4],
            [-x/2 + 69.75, -y/2 + 45.5],
            [x/2 - 4, -y/2 + 4],
            [x/2 - 63, -y/2 + 4],
            [-x/2 + 87.5, -y/2 + 4],
            [-x/2 + 87.5, y/2 - 4],
            [x/2 - 21, y/2 - 4,],
            [-x/2 + 148, y/2 - 4]
        ];
        //@SECTION: construction
        %difference()
        {
            translate([0, 0, standoff_z + z])
            linear_extrude(26.5)
                square([x, y], center = true);
            translate([x/2 - 58, y/2 - 13.5, standoff_z + z])
                cube([33, 13.6, 26.6]);
            translate([x/2 - 26, -y/2 - 1, standoff_z + z])
                cube([27, y + 2, 26.6]);
            translate([-x/2 + 73, -y/2 - 1, standoff_z + z])
                cube([18, y + 2, 26.6]);
            translate([-x/2 - 1, -y/2 - 1, standoff_z + z])
                cube([31, y + 2, 26.6]);
        }
        color("green")
        translate([0, 0, standoff_z])
        linear_extrude(z)
            square([x, y], center = true);
        difference()
        {
            color("black")
            translate([x/2 - 18, -5, standoff_z + z])
                cube([17, 59, 7]);
            translate([x/2 - 17, -4, standoff_z + z + 2])
                cube([15, 57, 6]);
        }
        %difference()
        {
            linear_extrude(standoff_z)
                square([x, y], center = true);
            for(p = standoff_points)
                #translate([p[0], p[1], -1])
                linear_extrude(2)
                    circle(d = 5.5);
        }
        difference()
        {
            color("black")
            translate([-x/2 + 80, y/2 - 52.5, standoff_z + z])
                cube([8.75, 25, 7]);
            translate([-x/2 + 84, y/2 - 50.5, standoff_z + z])
                cube([1.75, 21, 8]);
        }
        difference()
        {
            color("black")
            translate([-x/2 + 12.5, y/2 - 117, standoff_z + z])
                cube([8.75, 89, 7]);
            translate([-x/2 + 16.5, y/2 - 115, standoff_z + z])
                cube([1.75, 85, 8]);
        }
    }
    module hifive_unleashed()
    {
        //@SECTION: constants
        x = 120;
        y = 90;
        z = 1.6;
        standoff_z = 15;
        standoff_points = [
            [x/2 - 4, -y/2 + 4],
            [-x/2 + 4, -y/2 + 4]
        ];
        //@SECTION: construction
        %difference()
        {
            translate([0, 0, standoff_z + z])
            linear_extrude(25)
                square([x, y], center = true);
            translate([x/2 - 20, -y/2 - 1, standoff_z + z])
                cube([21, y + 2, 26]);
        }
        color("green")
        translate([0, 0, standoff_z])
        linear_extrude(z)
            square([x, y], center = true);
        translate([x/2 - 63, y/2 - 15, standoff_z - 8])
            cube([57, 15, 8]);
        %difference()
        {
            translate([0, -9, 0])
            linear_extrude(standoff_z)
                square([x, y - 18], center = true);
            for(p = standoff_points)
                #translate([p[0], p[1], -1])
                linear_extrude(2)
                    circle(d = 5);
        }
    }
    module bracket()
    {
        //@SECTION: constants
        z = 1;
        safe_zone = 3;
        //@SECTION: components
        module front()
        {
            //@SECTION: constants
            x = 18;
            y = 103.5;
            top_xshft = 3;
            top_y = 5;
            top_yoff = 2.5;
            tab_x = 10.5;
            tab_y = 6.5;
            tab_yoff = 4.5;
            //@SECTION: construction
            translate([-top_xshft, (y + top_y)/2 + top_yoff])
                square([x, top_y], center = true);
            translate([-x/2 - top_xshft, y/2 + top_yoff, 0])
                polygon([[0, 0], [x, 0],
                        [x + top_xshft, -top_yoff],
                        [top_xshft, -top_yoff]]);
            square([x, y], center = true);
            translate([-x/2, -y/2, 0])
                polygon([[0, 0], [x, 0],
                        [(x + tab_x)/2, -tab_yoff],
                        [(x - tab_x)/2, -tab_yoff]]);
            translate([0, -(y + tab_y)/2 - tab_yoff , 0])
                square([tab_x, tab_y], center = true);
        }
        module lip()
        {
            //@SECTION: constants
            x = 12;
            y = 18;
            z = 1;
            //@SECTION: construction
            color("white")
            linear_extrude(1)
            difference()
            {
                square([x, y], center = true);
                translate([0, -y/2 + 5.5 - 4.5/2, 0])
                    circle(4.5/2);
                translate([0, -y/2 + (4.5/2 + 1)/2, 0])
                    square([4.5, 4.5/2 + 1],
                            center = true);
            }
            %translate([(x + 3)/2, 0, 0.5])
                square([3, y], center = true);
        }
        //@SECTION: assembly
        %linear_extrude(safe_zone)
            front();
        color("white")
        translate([0, 0, safe_zone])
        linear_extrude(z)
            front();
        translate([-3, 60.25, 9])
        rotate(a = [90, 90, 0])
            lip();
    }
    module usb_card()
    {
        //@SECTION: constants
        x = 85;
        y = 52;
        z = 1.75;
        //@SECTION: construction
        color("green")
        linear_extrude(z)
            square([x, y], center = true);
        %translate([0, 0, z])
        linear_extrude(9)
            square([x, y], center = true);
        translate([x/2 - 30, -y/2 - 6.5, 0])
        linear_extrude(z)
            square([20, 13], center = true);
        //@SECTION: assembly
        translate([-x/2 + 2.25, 6.75, 8])
        rotate(a = [0, -90, 0])
            bracket();
    }
    module gfx_card()
    {
        //@SECTION: constants
        x = 144;
        y = 56;
        z = 1.75;
        safe_zone = 14;
        vga_x = 48;
        vga_y = 33;
        vga_z = 17;
        pci_x = 85;
        pci_y = 13;
        //@SECTION: construction
        %translate([0, y, 0])
        linear_extrude(vga_z)
            square([vga_x, vga_y]);
        %translate([0, 0, z])
        linear_extrude(safe_zone)
            square([x, y]);
        color("green")
        linear_extrude(z)
            square([x, y]);
        translate([44, -pci_y, 0])
        linear_extrude(z)
            square([pci_x, pci_y]);
        //@SECTION: assembly
        translate([2, 35.25, 8])
        rotate(a = [0, -90, 0])
            bracket();
    }
    //@SECTION: assembly
    expansion_board();
    translate([164, -1, 14])
    rotate(a = [0, 0, 90])
        hifive_unleashed();
    translate([-50.25, 33, 62])
    rotate(a = [90, 0, -90])
        usb_card();
    translate([-117.75, 74, 33.5])
    rotate(a = [90, 0, -90])
        gfx_card();
}
module case_bottom()
{
    //@SECTION: constants
    //@NOTE: changing these will break things; assumptions are baked in to model
    wall_z = 4;
    foot_z = 12;
    //@SECTION: components
    module base()
    {
        //@SECTION: construction
        let(x = 272,
            y = 120,
            standoff_points = [
                [-x/2 + 7, -y/2 + 4],
                [-x/2 + 7, y/2 - 4],
                [-x/2 + 7, -y/2 + 45.5],
                [-x/2 + 31.5, -y/2 + 4],
                [-x/2 + 31.5, y/2 - 4],
                [-x/2 + 31.5, -y/2 + 45.5],
                [-x/2 + 69.75, -y/2 + 4],
                [-x/2 + 69.75, y/2 - 4],
                [-x/2 + 69.75, -y/2 + 45.5],
                [x/2 - 4, -y/2 + 4],
                [x/2 - 63, -y/2 + 4],
                [-x/2 + 87.5, -y/2 + 4],
                [-x/2 + 87.5, y/2 - 4],
                [x/2 - 21, y/2 - 4,],
                [-x/2 + 148, y/2 - 4]])
        {
            difference()
            {
                translate([-2, 0, 0])
                linear_extrude(wall_z)
                    square([278, 151]);
                for(p = standoff_points)
                    translate([p[0] + 138.5, p[1] + 72.5, -1])
                    {
                        linear_extrude(wall_z + 2)
                            circle(d = 3);
                        linear_extrude(wall_z - 1)
                            circle(d = 8);
                    }
                for(i = [12:4:134])
                    translate([42, i, -1])
                    linear_extrude(wall_z + 2)
                        square([22.25, 2]);
            }
        }
        let(x = 120,
            y = 90,
            standoff_points = [
                [x/2 - 4, -y/2 + 4],
                [-x/2 + 4, -y/2 + 4]])
        {
            difference()
            {
                translate([294, 0, 14])
                linear_extrude(wall_z)
                    square([70, 145]);
                translate([164 + 138.5, -1 + 72.5, 14])
                rotate(a = [0, 0, 90])
                union()
                {
                    for(p = standoff_points)
                        translate([p[0], p[1], -1])
                        {
                            linear_extrude(wall_z + 2)
                                circle(d = 3);
                            linear_extrude(wall_z - 1)
                                circle(d = 8);
                        }
                }
            }
        }
        //@SECTION: assembly
        translate([276, 151/2 - 4, 18])
        rotate([90, 0, 0])
        let(r = 18, length = 151, z = wall_z)
        {
            intersection()
            {
                difference()
                {
                    cylinder(h = length, r = r, center = true);
                    cylinder(h = length + 1, r = r - z, center = true);
                }
                translate([0, -r, -(length + 1)/2])
                linear_extrude(length + 1)
                    square([r, r]);
            }
        }
    }
    module foot()
    {
        //@SECTION: construction
        linear_extrude(foot_z)
        intersection()
        {
            difference()
            {
                circle(d = 42);
                circle(d = 42 - 4);
            }
            square([42, 42]);
        }
    }
    module tab_foot()
    {
        //@SECTION: construction
        difference()
        {
            translate([-1.25, -1.25, 0])
            cube([15.5, wall_z + 0.5, 12.5]);
            translate([1, 1, 0])
                cube([11, 1.5, 13.5]);
        }
    }
    module bracket_wall_detail()
    {
        translate([44.25, 171, 30.56])
        difference()
        {
            cube([1.5, 0.5, 17.44]);
            translate([0, 1, 0])
            rotate([90, 0, 0])
            linear_extrude(wall_z + 1)
                polygon([[0, -1], [2.5, -1], [2.5, 1.75],
                    [1.5, 1.75], [0, 0]]);
        }
        translate([25.25, 171.5, 30.56])
        rotate([0, 0, 180])
        difference()
        {
            cube([1.5, 0.5, 17.44]);
            translate([0, 1, 0])
            rotate([90, 0, 0])
            linear_extrude(wall_z + 1)
                polygon([[0, -1], [2.5, -1], [2.5, 1.75],
                    [1.5, 1.75], [0, 0]]);
        }
        difference()
        {
            translate([40.25, 171.5, 28])
            rotate([90, 0, 0])
            linear_extrude(wall_z + 0.5)
                polygon([[0, 0], [4, 4.75], [4, 0]]);
            translate([41.25, 172, 27])
            rotate([90, 0, 0])
            linear_extrude(wall_z + 2)
                polygon([[0, 0], [4, 4.75], [4, 0]]);
        }
        difference()
        {
            translate([29.25, 167, 28])
            rotate([90, 0, 180])
            linear_extrude(wall_z + 0.5)
                polygon([[0, 0], [4, 4.75], [4, 0]]);
            translate([28.25, 169, 27])
            rotate([90, 0, 180])
            linear_extrude(wall_z + 2)
                polygon([[0, 0], [4, 4.75], [4, 0]]);
        }
    }
    //@SECTION: construction
    difference()
    {
        union()
        {
            translate([112.75, 167, 12])
            difference()
            {
                translate([122, wall_z, 37 - 16])
                    cube([138, 0.5, 15]);
                translate([124, 0, 37 - 14])
                    cube([134, wall_z + 2, 14]);
            }
            bracket_wall_detail();
            translate([67.5, 0, 0])
                bracket_wall_detail();
            difference()
            {
                translate([16, 22, 12])
                    cube([wall_z, 145, 36]);
                translate([18, (149+20)/2, 32])
                    cube([3, 20, 3]);
            }
            difference()
            {
                translate([16, 18, 12])
                    cube([374, wall_z, 36]);
                translate([(374+20)/4, 20, 32])
                    cube([20, 3, 3]);
                translate([374 - (374-20)/4, 20, 32])
                    cube([20, 3, 3]);
            }
            difference()
            {
                translate([16, 167, 12])
                    cube([96.75, wall_z, 36]);
                translate([29.25, 169.25, 16])
                        cube([11, 1.5, 13.5]);
                translate([96.75, 169.25, 16])
                        cube([11, 1.5, 13.5]);
                difference()
                {
                    translate([25.25, 166, 28])
                        cube([19, wall_z + 2, 21]);
                    translate([41.25, 172, 27])
                    rotate([90, 0, 0])
                    linear_extrude(wall_z + 2)
                        polygon([[0, 0], [4, 4.75], [4, 0]]);
                    translate([28.25, 166, 27])
                    rotate([90, 0, 180])
                    linear_extrude(wall_z + 2)
                        polygon([[0, 0], [4, 4.75], [4, 0]]);
                }
                translate([67.5, 0, 0])
                difference()
                {
                    translate([25.25, 166, 28])
                        cube([19, wall_z + 2, 21]);
                    translate([41.25, 172, 27])
                    rotate([90, 0, 0])
                    linear_extrude(wall_z + 2)
                        polygon([[0, 0], [4, 4.75], [4, 0]]);
                    translate([28.25, 166, 27])
                    rotate([90, 0, 180])
                    linear_extrude(wall_z + 2)
                        polygon([[0, 0], [4, 4.75], [4, 0]]);
                }
            }
            translate([112.75, 167, 12])
            difference()
            {
                cube([273.25, wall_z, 36]);
                translate([124, -1, 37 - 14])
                    cube([134, wall_z + 2, 14]);
                translate([51, -1, 32-12])
                    cube([20, 3, 3]);
            }
            difference()
            {
                translate([386, 22, 12])
                    cube([wall_z, 149, 36]);
                translate([385, (149+20)/2, 32])
                    cube([3, 20, 3]);
            }
        }
        difference()
        {
            translate([15, 17, 45])
                cube([376, 155, 3]);
            translate([18, 20, 45])
                cube([370, 149.5, 3]);
        }
    }
    translate([22, 22, 12])
    linear_extrude(wall_z)
    difference()
    {
        minkowski()
        {
            square([365, 151]);
            circle(d = 42);
        }
        square([365, 149]);
    }
    //@SECTION: assembly
    translate([28.25, 168.25, 16])
        tab_foot();
    translate([95.75, 168.25, 16])
        tab_foot();
    translate([22, 22, foot_z])
        base();
    translate([23, 23, 0])
    rotate([0, 0, -180])
        foot();
    translate([23, 172, 0])
    rotate([0, 0, 90])
        foot();
    translate([386, 23, 0])
    rotate([0, 0, -90])
        foot();
    translate([386, 172, 0])
    rotate([0, 0, 0])
        foot();

}
module case_top()
{
    //@SECTION: constants
    wall_z = 4;
    mount_points = [[27.5, 157.5, 91],
        [95.0, 157.5, 91]];
    //@SECTION: components
    module support(length = 9.25)
    {
        rotate([0, -90, 0])
        linear_extrude(length)
            polygon([[0, 0], [12, 0], [12, 12]]);
    }
    module lug()
    {
        difference()
        {
            translate([0, 0, -2.5])
                cube([7.75, 7.75, 2.5]);
            translate([2, 2, -3])
                cube([6, 6, 6]);
        }
    }
    module snap(rot_z = 0)
    {
        rotate([90, 0, rot_z])
        linear_extrude(20)
            polygon([
                [0, 0],
                [3, 0],
                [5, 2],
                [5, 2.5],
                [3, 2.5],
                [3, 28],
                [0, 25]]);
    }
    //@SECTION: construction
    difference()
    {
        cube([374, 153, 96]);
        translate([wall_z, wall_z, -1])
            cube([374 - 2*wall_z, 153 - 2*wall_z, 96 + 1 - wall_z ]);
        translate([220.75, 148, -1])
            cube([134, wall_z + 2, 14]);
        translate([9.25, 148, -1])
            cube([19, wall_z + 2, 90]);
        translate([9.25, 148, 88.5])
            cube([22, wall_z + 2, 9]);
        translate([76.75, 148, -1])
            cube([19, wall_z + 2, 90]);
        translate([76.75, 148, 88.5])
            cube([22, wall_z + 2, 9]);
    }
    difference()
    {
        translate([218.75, 153, 0])
            cube([138, 0.5, 15]);
        translate([220.75, 148, -1])
            cube([134, wall_z + 2, 14]);
    }
    translate([28.25, 153, 0])
        cube([1.5, 0.5, 88.5]);
    translate([7.75, 153, 0])
        cube([1.5, 0.5, 80]);
    translate([95.75, 153, 0])
        cube([1.5, 0.5, 88.5]);
    translate([75.25, 153, 0])
        cube([1.5, 0.5, 92]);
    difference()
    {
        translate([0, 153, 96 - wall_z])
            cube([108.25, 12, wall_z]);
        for(i = mount_points)
        {
            translate([i[0], i[1], i[2]])
            {
                linear_extrude(6)
                    circle(d = 3);
                linear_extrude(3)
                    circle(d = 8);
            }
        }
    }
    translate([82.5, 4, 0])
        cube([20, 0.5, 12]);
    translate([269.5, 4, 0])
        cube([20, 0.5, 12]);
    translate([147.5, 148.5, 0])
        cube([20, 0.5, 12]);
    translate([4, 66.5, 0])
        cube([0.5, 20, 12]);
    translate([369.5, 66.5, 0])
        cube([0.5, 20, 12]);
    //@SECTION: assembly
    translate([75, 153, 80])
        support(42);
    translate([9.25 , 153, 80])
        support();
    translate([108.25, 153, 80])
        support(8);
    lug();
    translate([374, 0, 0])
    rotate([0, 0, 90])
        lug();
    translate([374, 153, 0])
    rotate([0, 0, 180])
        lug();
    translate([0, 153, 0])
    rotate([0, 0, -90])
        lug();
    translate([366.5, 86.5, -16])
        snap();
    translate([7.5, 66.5, -16])
        snap(180);
    translate([147.5, 145.5, -16])
        snap(90);
    translate([289.5, 7.5, -16])
        snap(-90);
    translate([102.5, 7.5, -16])
        snap(-90);
}
//@SECTION: assembly
translate([160.5, 94.5, 16.5])
    guts();
color("dimgray")
    case_bottom();
%translate([16, 18, 48])
/* translate([0, 165, 96]) */
/* rotate([180, 0, 0]) */
    case_top();
